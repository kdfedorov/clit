#pragma once
/* FILE: include/wndSettings.h
   DESC: Neural network settings window */

#include "shared.h"

#include <array>
#include <QDialog>
#include <QLabel>
#include <QComboBox>
#include <QSpinBox>
#include <QPushButton>

namespace clit {
   class wndSettings final : public QDialog {
      Q_OBJECT
   public:
      wndSettings(SettingsStruct& settings, QWidget* parent = nullptr, int w = 300, int h = 230);
      ~wndSettings();

      ///////////////////////////////
      // Widget handlers

      // Changed algorithm
      void OnAlgorithmChanged(int index);
      // Pressed apply button
      void OnApply();
      // Pressed cancel button
      void OnCancel();

   private:
      // Connects interface elements to functions
      void connect();

      // Initializes parameters widgets
      void initParameters();

      // Handles resizing
      void resizeEvent(QResizeEvent* event) override;

      // Collects common network settings
      void collectCommon();
      // Collects special network parameters
      void collectSpecial();
      // Collects settings of currently widget stored parameters
      void collectTemp();
      // Collects settings
      void collect();

      // Resets common widget values
      void resetCommon();
      // Resets special widget value
      void resetSpecial();
      // Resets widget value
      void reset();

      // Disconnects interface elements to functions
      void disconnect();

   private:
      // Available parameters
      enum Parameters {
         ParameterAlgorithm,
         ParameterMetric,
         ParameterErasCount,
         ParameterThreshold,
         ParameterRate,
         ParameterDownturn,

         ParameterSpecial,

         ParametersCount
      };

      // Interfaces
      enum ParameterInterface {
         IfcCombo,
         IfcSpin,
         IfcDoubleSpin,

         IfcCount
      };

      // Parameter struct
      struct Parameter {
         QLabel*              label    = nullptr;
         QWidget*             widget   = nullptr;
         ParameterInterface   ifc      = IfcCount;
         QSize                size     = QSize(0, 0);
         QSize                lsize    = QSize(0, 0);
      };

   private:
      // Get parameter as spin box (quick access)
      QSpinBox* getAsSpin(Parameters p);
      // Get parameter as double spin box (quick access)
      QDoubleSpinBox* getAsDoubleSpin(Parameters p);
      // Get parameter as combo box (quick access)
      QComboBox* getAsCombo(Parameters p);

   private:
      // Array of parameters widgets
      std::array <Parameter, ParametersCount> m_arrParameters;
      // Buttons
      QPushButton* m_btnApply;
      QPushButton* m_btnCancel;
      
      // Actual data
      SettingsStruct& m_Data;
      SettingsStruct  m_TempSettings;
      NetworkParams*  m_curSettings;
   };
}
