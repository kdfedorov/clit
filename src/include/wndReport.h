﻿#pragma once
/* FILE: include/wndReport.h
   DESC: Clustering results report window. */

#include "shared.h"

#include <QDialog>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPushButton>
#include <QLabel>

namespace clit {
   class wndReport final : public QDialog {
      Q_OBJECT

      static constexpr int defaultWidth = 400;
      static constexpr int defaultHeight = 600;

      // Drawing info
      struct ImageInfo {
         QPainter*   painter  = nullptr;
         QImage*     img      = nullptr;
         int         objc     = 0;
         int         imgc     = 0;
      };

   public:
      wndReport(const clustlib::cluster_vec& cv, QWidget* parent = nullptr,
         int w = defaultWidth, int h = defaultHeight);
      wndReport(const clustlib::cluster_tree& ct, QWidget* parent = nullptr,
         int w = defaultWidth, int h = defaultHeight);
      ~wndReport();

      // Exporting PDF action handler
      void OnPressedPDF();
      // Exporting CSV action handler
      void OnPressedCSV() noexcept(false);

      // Go to next page
      void OnNextPage();
      // Go to prev page
      void OnPrevPage();

   private:
      // Common code of constructors
      void commonConstruct(int w, int h);
      // Connects interface elements from functions
      void connect();

      // Draws report
      void makeReport() noexcept(false);
      // Draws cluster tree report
      void makeClusterTreeReport(ImageInfo& info);
      // Draw cluster tree level
      void drawClusterTreeLevel(ImageInfo& info, const clustlib::cluster_tree& cluster, int level, int count);
      // Draws cluster vector report
      void makeClusterVecReport(ImageInfo& info);
      // Draws cluster
      void drawCluster(ImageInfo& info, const clustlib::cluster& cluster, int maj, int min = -1);
      // Draws object
      void drawObject(ImageInfo& info, const clustlib::object& object);
      // Draws gap
      void drawGap(ImageInfo& info);
      // Checks if page is over
      bool isPageOver(ImageInfo& info);
      // Gets new page
      void getNewPage(ImageInfo& info);
      // Returns printing object bounding rect
      void getObjectRect(int objc, QRect& rect);

      // Saves cluster tree to directory
      void saveClusterTree(const QString& strDir);
      // Saves cluster tree level to directory
      void saveClusterTreeLevel(const QString& strDir, const clustlib::cluster_tree& ct,
         int level, int count);
      // Saves cluster vector to directory;
      void saveClusterVec(const QString& strDir);

      // Shows page to scene
      void changePage();

      // Handles resizing
      void resizeEvent(QResizeEvent* event) override;

      // Disconnects interface elements from functions
      void disconnect();

   private:

      //// GUI Elements /////////////////////////////////////
      QPushButton*      m_btnPDF;         // Export PDF button
      QPushButton*      m_btnCSV;         // Export CSV button
      QGraphicsView*    m_viewReport;     // Report's graphics view
      QGraphicsScene*   m_graphicsReport; // Report's graphics scene

      // Page elements
      QPushButton*   m_btnPrev;     // Go to previous page
      QPushButton*   m_btnNext;     // Go to next page
      QLabel*        m_labelPage;   // Current page label

      // Report pages
      std::vector <QImage> m_vPages;
      // Current page
      int m_nCurrentPage = 0;

      //// Data /////////////////////////////////////////////
      const clustlib::cluster_vec*  m_vecClusters  = nullptr;
      const clustlib::cluster_tree* m_treeClusters = nullptr;
   };
}
