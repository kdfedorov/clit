#pragma once
/* FILE: include/wndMain.h
   DESC: Application's main window */

#include <array>
#include "shared.h"

#include <QMainWindow>
#include <QPushButton>
#include <QTableWidget>


namespace clit {
   class wndMain final : public QMainWindow {
      Q_OBJECT
   public:
      wndMain(int width = 640, int height = 480);
      ~wndMain();

      // === Handling functions =======
      // Executes clustering
      void OnButtonLaunch();
      // Opens settings dialog
      void OnButtonSettings();

      // === File functions ===========
      // Handles new file call
      void OnNewFile();
      // Handles open file call
      void OnOpenFile();
      // Handles save file call
      void OnSaveFile();
      // Handles save as.. call
      void OnSaveAs();

      // === Table functions ==========
      // Handles add row call
      void OnAddRow();
      // Handles add column call
      void OnAddColumn();
      // Handles remove row call
      void OnRemoveRow();
      // Handles remove column call
      void OnRemoveColumn();

   private:
      // Connects interface elements to functions
      void connect();

      // Initializes application menubar
      void initMenubar();
      // Initializes menu actions
      void initActions();
      // Initializes file group menu actions
      void initFileGroup();
      // Initializes table group menu actions
      void initTableGroup();

      // Updates table content
      void updateTable();

      // Marks current file as saved
      void markAsSaved();
      // Marks current file as unsaved
      void markAsUnsaved();
      // Handles unsaved file
      bool handleUnsaved();

      // Handles changing cells
      void onCellChanged(int row, int column);
      // Handles resizing
      void resizeEvent(QResizeEvent* event) override;

      // Disconnects interface elements from functions
      void disconnect();

   private:
      // === GUI elements ===============
      QTableWidget*  m_tableDataset;   // Dataset table
      QPushButton*   m_btnLaunch;      // Clustering button
      QPushButton*   m_btnSettings;    // Clustering parameters button

      QMenuBar*      m_barMenu;        // Application menubar

      // Menubar actions ////////////////
      // Menu groups
      enum ActionGroup {
         FileGroup,
         TableGroup,
         
         ActionGroupsCount
      };

      // Actions vector
      using ActionsVector = std::vector <QAction*>;
      // Actions group array
      using GroupsArray = std::array <ActionsVector, ActionGroupsCount>;

      // File group actions
      enum FileActions {
         NewFileAction,
         OpenFileAction,
         SaveFileAction,
         SaveAsAction,

         FileActionsCount
      };

      // Table group actions
      enum TableActions {
         AddRowAction,
         AddColumnAction,
         RemoveRowAction,
         RemoveColumnAction,

         TableActionsCount
      };

      // Current menubar actions
      GroupsArray m_arrActions;
      
      // === Application data ===========
      SettingsStruct m_Data;              // Clustering settings

      clustlib::csv  m_csvManager;        // File manager
      QString        m_strFile = "";      // Current file
      bool           m_bSaved = true;     // Checking saving file state
      bool           m_bUpdate = false;   // Updating table state
      ///////////////////////////////////
   };
}
