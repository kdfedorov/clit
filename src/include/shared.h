﻿#pragma once
#include <array>
#include <clustlib.h>

namespace clit {
   // Neural network parameters
   struct NetworkParams {
      NetworkParams() = default;
      // Base neural network settings
      clustlib::neural_network::settings m_CommonSettings;
      clustlib::number_t   m_nClusters = 0;  // Number of clusters
      clustlib::value_t    m_dCritDistance = 0;  // Critical distance
   };

   // clustlib algorithms count
   constexpr auto countAlgorithms = static_cast <int> (clustlib::algorithms::alg_hierarchical_kohonen) + 1; // :p
   // clustlib networks parameters protocol
   using NetworkParamsArray = std::array <NetworkParams, countAlgorithms>;

   // Struct used to communicate with settings window
   struct SettingsStruct {
      SettingsStruct() {
         m_funcMetric = clustlib::euclidean;
         m_algCurrent = clustlib::algorithms::alg_kohonen;
         for (auto& np : m_Settings) {
            np.m_CommonSettings.eras = 0;
            np.m_CommonSettings.threshold = 0.0;
            np.m_CommonSettings.learning_rate = 0.0;
            np.m_CommonSettings.rate_downturn = 0.0;
            np.m_dCritDistance = 0.0;
            np.m_nClusters = 0;
         }
      }

      NetworkParamsArray   m_Settings;    // Neural networks settings
      clustlib::metric_f   m_funcMetric;  // Used metric function
      clustlib::algorithms m_algCurrent;  // Used clustering algorithm
   };
}
