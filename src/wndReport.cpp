#include "include/wndReport.h"

#include <cmath>
#include <stdexcept>

#include <QResizeEvent>
#include <QPrinter>
#include <QFileDialog>
#include <QMessageBox>

namespace clit {
   namespace utils {
      static constexpr auto dpi = 75;
      static constexpr auto mmpi = 25.4f;

      constexpr inline int mm_to_pt(int mm) {
         return mm / mmpi * dpi;
      }

      constexpr inline int pt_to_mm(int pt) {
         return pt / dpi * mmpi;
      }

      constexpr auto widthA4 = mm_to_pt(210);
      constexpr auto heightA4 = mm_to_pt(297);
      static const QSize sizeA4(widthA4, heightA4);

      constexpr auto colorInk    = Qt::black;
      constexpr auto colorList   = Qt::white;

      constexpr auto imageFormat = QImage::Format::Format_RGB32;

      constexpr auto marginVert = mm_to_pt(5);
      constexpr auto marginHor = mm_to_pt(3);

      constexpr auto gapLine = 13;
      constexpr auto maxLine = (heightA4 - (2 * marginVert)) / (gapLine);

      static inline QImage GetEmptyPage() {
         static QImage imgEmpty(sizeA4, imageFormat);
         imgEmpty.fill(colorList);
         return imgEmpty;
      }

      static inline QFont GetFont() {
         static QFont font("Monospace", 9);
         font.setStyleHint(QFont::TypeWriter);
         return font;
      }

      static inline QPen GetPen() {
         static QPen pen(colorInk, 1);
         return pen;
      }
   }

   wndReport::wndReport(const clustlib::cluster_vec& cv, QWidget* parent/*= nullptr*/,
      int w/* = defaultWidth*/, int h/*= defaultHeight*/) {
      m_vecClusters = &cv;
      commonConstruct(w, h);
   }

   wndReport::wndReport(const clustlib::cluster_tree& ct, QWidget* parent/*= nullptr*/,
      int w/* = defaultWidth*/, int h/*= defaultHeight*/) {
      m_treeClusters = &ct;
      commonConstruct(w, h);
   }

   wndReport::~wndReport() {
      disconnect();
      hide();
      delete m_btnCSV;
      delete m_btnPDF;
      delete m_graphicsReport;
      delete m_viewReport;

      delete m_btnNext;
      delete m_btnPrev;
      delete m_labelPage;
   }

   void wndReport::OnPressedPDF() {
      QString filename = QFileDialog::getSaveFileName(this, QString("Сохранить в PDF"), QString(), "*.pdf");
      if (filename.isEmpty()) {
         QMessageBox::information(this, QString("Экспорт не удался"), QString("Не удалось получить доступ к директории."));
         return;
      }
      else if (QFileInfo(filename).suffix().isEmpty())
         filename.append(".pdf");

      QPrinter printer(QPrinter::PrinterResolution);
      printer.setOutputFormat(QPrinter::PdfFormat);
      printer.setPaperSize(QPrinter::A4);
      printer.setOutputFileName(filename);

      QPainter painter;
      painter.begin(&printer);
      for (const auto& page : m_vPages) {
         int pgw = page.width();
         int prw = printer.paperRect().width();
         int x = (prw - pgw) / 2;
         painter.drawImage(QPoint(x, 10), page);
         painter.resetTransform();
         if (page != *m_vPages.rbegin())
            printer.newPage();
      }
      painter.end();
   }

   void wndReport::OnPressedCSV() noexcept(false) {
      QString dir = QFileDialog::getExistingDirectory(this, QString("Сохранить в директорию"), QString());
      if (dir.isEmpty()) {
         QMessageBox::information(this, QString("Экспорт не удался"), QString("Не удалось получить доступ к директории."));
         return;
      }

      if (m_vecClusters)
         saveClusterVec(dir);
      else if (m_treeClusters)
         saveClusterTree(dir);
      else
         throw std::logic_error("No clusters result was found. This is not right.");
   }

   void wndReport::OnNextPage() {
      if (m_nCurrentPage == m_vPages.size())
         return;

      m_nCurrentPage++;
      changePage();
   }

   void wndReport::OnPrevPage() {
      if (m_nCurrentPage == 1)
         return;

      m_nCurrentPage--;
      changePage();
   }

   void wndReport::connect() {
      QObject::connect(m_btnPDF, &QPushButton::clicked, this, &wndReport::OnPressedPDF);
      QObject::connect(m_btnCSV, &QPushButton::clicked, this, &wndReport::OnPressedCSV);
      QObject::connect(m_btnNext, &QPushButton::clicked, this, &wndReport::OnNextPage);
      QObject::connect(m_btnPrev, &QPushButton::clicked, this, &wndReport::OnPrevPage);
   }

   void wndReport::commonConstruct(int w, int h) {
      this->setMinimumSize(280, 100);
      this->setWindowTitle(QString("clit - Отчёт"));

      m_btnPDF = new QPushButton(QString("Экспорт в PDF"), this);
      m_btnCSV = new QPushButton(QString("Экспорт в CSV"), this);

      m_btnNext = new QPushButton(QString("->"), this);
      m_btnPrev = new QPushButton(QString("<-"), this);
      m_labelPage = new QLabel(QString("0/0"), this);

      m_graphicsReport = new QGraphicsScene(this);
      m_graphicsReport->setBackgroundBrush(Qt::gray);
      m_viewReport = new QGraphicsView(m_graphicsReport, this);

      this->resize(w, h);
      connect();
      makeReport();
   }

   void wndReport::makeReport() noexcept(false) {
      m_vPages.clear();
      ImageInfo info;
      getNewPage(info);

      if (m_vecClusters)
         makeClusterVecReport(info);
      else if (m_treeClusters)
         makeClusterTreeReport(info);
      else
         throw std::logic_error("No clusters result was found. This is not right.");
      
      if (!m_vPages.empty()) {
         m_nCurrentPage = 1;
         changePage();
      }

      if (info.painter)
         info.painter->end();
   }

   void wndReport::makeClusterTreeReport(ImageInfo& info) {
      drawClusterTreeLevel(info, *m_treeClusters, 1, 1);
   }

   void wndReport::drawClusterTreeLevel(ImageInfo& info, const clustlib::cluster_tree& cluster,
      int level, int count) {
      drawCluster(info, cluster.parent, level, count);

      int child_count = 1;
      for (const auto& child : cluster.children)
         drawClusterTreeLevel(info, child, level + 1, child_count++);
   }

   void wndReport::makeClusterVecReport(ImageInfo& info) {
      int count = 1;
      for (const auto& cluster : *m_vecClusters)
         drawCluster(info, cluster, count++);
   }

   void wndReport::drawCluster(ImageInfo& info, const clustlib::cluster& cluster, int maj, int min/* = -1*/) {
      ImageInfo infoNext;
      infoNext.objc = info.objc + static_cast <int> (std::round(std::min(double(cluster.attribute_count()), double(4))));
      if (isPageOver(infoNext))
         getNewPage(info);

      QString name = QString("Кластер #") + QString().number(maj);
      if (min != -1)
         name += '.' + QString().number(min);

      QRect rectName;
      getObjectRect(info.objc++, rectName);
      info.painter->drawText(rectName, name);
      for (const auto& object : cluster.get_dataset())
         drawObject(info, object);

      drawGap(info);
   }

   void wndReport::drawObject(ImageInfo& info, const clustlib::object& object) {
      if (isPageOver(info))
         getNewPage(info);

      QRect rectObj;
      getObjectRect(info.objc, rectObj);
      info.painter->drawRect(rectObj);
      auto widthCell = rectObj.width() / object.size();

      int count = 0;
      for (auto val : object.data) {
         QString str = QString().number(val, 103, 3);
         info.painter->drawText(QRect(
            QPoint(utils::marginHor + (count * widthCell), rectObj.top()),
            QPoint(utils::marginHor + ((count + 1) * widthCell), rectObj.bottom())),
         str, QTextOption(Qt::AlignCenter));
         
         count += 1;
         info.painter->drawLine(
            QPoint(utils::marginHor + (count * widthCell), rectObj.top()),
            QPoint(utils::marginHor + (count * widthCell), rectObj.bottom())
         );
      }

      info.objc++;
   }

   void wndReport::drawGap(ImageInfo& info) {
      if (isPageOver(info))
         getNewPage(info);
      else
         info.objc++;
   }

   bool wndReport::isPageOver(ImageInfo& info) {
      return info.objc > utils::maxLine;
   }

   void wndReport::getNewPage(ImageInfo& info) {
      m_vPages.push_back(utils::GetEmptyPage());

      info.imgc = m_vPages.size() - 1;
      info.img = &m_vPages[info.imgc];
      info.objc = 0;

      if (!info.painter)
         info.painter = new QPainter();
      else 
         info.painter->end();

      info.painter->begin(info.img);
      info.painter->setFont(utils::GetFont());
      info.painter->setPen(utils::GetPen());
   }

   void wndReport::getObjectRect(int objc, QRect& rect) {
      rect = QRect(
         QPoint(utils::marginHor, utils::marginVert + (objc * utils::gapLine)),
         QPoint(utils::widthA4 - utils::marginHor, utils::marginVert + ((objc + 1) * utils::gapLine))
      );
   }

   void wndReport::saveClusterTree(const QString& strDir) {
      saveClusterTreeLevel(strDir, *m_treeClusters, 0, 0);
      QMessageBox::information(this, QString("Экспорт завершён"), QString("Файлы успешно сохранены!"));
   }

   void wndReport::saveClusterTreeLevel(const QString& strDir, const clustlib::cluster_tree& ct,
      int level, int count) {
      QString filename = strDir + "/cluster" + '_' + QString().number(level) + '_' + QString().number(count) + ".csv";
      if (!clustlib::csv::write(filename.toStdString().c_str(), ct.parent.get_dataset())) {
         QMessageBox::information(this, QString("Экспорт не удался"), QString("Не удалось сохранить файл."));
         return;
      }

      int count_child = 0;
      for (const auto& child : ct.children)
         saveClusterTreeLevel(strDir, child, level + 1, count_child++);
   }

   void wndReport::saveClusterVec(const QString& strDir) {
      int count = 0;
      for (const auto& cluster : *m_vecClusters) {
         QString filename = strDir + "/cluster" + QString().number(count) + ".csv";
         if (!clustlib::csv::write(filename.toStdString().c_str(), cluster.get_dataset())) {
            QMessageBox::information(this, QString("Экспорт не удался"), QString("Не удалось сохранить файл."));
            return;
         }

         count++;
      }

      QMessageBox::information(this, QString("Экспорт завершён"), QString("Файлы успешно сохранены!"));
   }

   void wndReport::changePage() {
      m_labelPage->setText(QString().number(m_nCurrentPage) + '/' + QString().number(m_vPages.size()));
      if (m_vPages.empty())
         return;

      QPixmap pixmapBlank;
      pixmapBlank.convertFromImage(m_vPages[m_nCurrentPage - 1]);
      m_graphicsReport->addPixmap(pixmapBlank);
   }

   void wndReport::resizeEvent(QResizeEvent* event) {
      constexpr int margin = 7;
      constexpr int heightBtn = 25;
      constexpr int widthBtn = 80;

      const QSize& size = event->size();
      m_viewReport->move(margin, margin);
      m_viewReport->resize(size.width() - 2 * margin, size.height() - (margin * 3 + heightBtn));

      m_btnPDF->move(size.width() - (margin + widthBtn), size.height() - (margin + heightBtn));
      m_btnPDF->resize(widthBtn, heightBtn);
      m_btnCSV->move(size.width() - 2 * (margin + widthBtn), size.height() - (margin + heightBtn));
      m_btnCSV->resize(widthBtn, heightBtn);

      m_btnPrev->resize(heightBtn, heightBtn);
      m_btnPrev->move(margin, size.height() - (margin + heightBtn));
      m_btnNext->resize(heightBtn, heightBtn);
      m_btnNext->move(2 * margin + heightBtn, size.height() - (margin + heightBtn));

      m_labelPage->move(3 * margin + 2 * heightBtn, size.height() - (m_labelPage->height() + margin));
   }

   void wndReport::disconnect() {
      QObject::disconnect(m_btnPDF, &QPushButton::clicked, this, &wndReport::OnPressedPDF);
      QObject::disconnect(m_btnCSV, &QPushButton::clicked, this, &wndReport::OnPressedCSV);
      QObject::disconnect(m_btnNext, &QPushButton::clicked, this, &wndReport::OnNextPage);
      QObject::disconnect(m_btnPrev, &QPushButton::clicked, this, &wndReport::OnPrevPage);
   }
}
