#include "include/wndMain.h"
#include <QApplication>

int main(int argc, char *argv[]) {
   QApplication app(argc, argv);
   clit::wndMain wnd;
   wnd.show();
   return app.exec();
}
