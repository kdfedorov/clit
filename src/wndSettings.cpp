#include "include/wndSettings.h"
#include <cmath>
#include <QResizeEvent>
namespace clit {
   namespace utils {
      enum Metrics {
         euclidean,
         sqeuclidean,
         manhattan,
         chebyshev,
      };

      int getMetricInt(Metrics m) {
         return static_cast <int> (m);
      }

      Metrics getMetricEnum(int i) {
         return static_cast <Metrics>(i);
      }

      clustlib::metric_f getMetric(Metrics m) {
         switch (m) {
            case sqeuclidean: return clustlib::euclidean_sq;
            case manhattan: return clustlib::manhattan;
            case chebyshev: return clustlib::chebyshev;
            default: return clustlib::euclidean;
         }
      }

      Metrics getMetric(clustlib::metric_f m) {
         if (m == clustlib::euclidean_sq)
            return sqeuclidean;
         if (m == clustlib::manhattan)
            return manhattan;
         if (m == clustlib::chebyshev)
            return chebyshev;

         return euclidean;
      }

      int getAlgInt(clustlib::algorithms alg) {
         return static_cast <int> (alg);
      }

      clustlib::algorithms getAlgEnum(int i) {
         return static_cast <clustlib::algorithms> (i);
      }

      bool isDistanceAlg(clustlib::algorithms alg) {
         return alg != clustlib::algorithms::alg_kohonen;
      }
   }

   wndSettings::wndSettings(SettingsStruct& settings, QWidget* parent/*= nullptr*/,
      int w/*= 300*/, int h/*= 230*/) : QDialog(parent), m_Data(settings), m_TempSettings(settings),
      m_curSettings(&m_TempSettings.m_Settings[static_cast <int> (m_TempSettings.m_algCurrent)]) {
      this->setMinimumSize(280, 230);
      this->setWindowTitle(QString("clit - Настройки"));
      m_btnApply = new QPushButton(QString("Применить"), this);
      m_btnCancel = new QPushButton(QString("Отменить"), this);
      initParameters();
      this->resize(w, h);

      m_btnApply->show();
      m_btnCancel->show();

      connect();
   }

   wndSettings::~wndSettings() {
      disconnect();

      for (auto& parameter : m_arrParameters) {
         parameter.label->hide();
         delete parameter.label;
         
         parameter.widget->hide();
         delete parameter.widget;
      }

      m_btnApply->hide();
      delete m_btnApply;
      m_btnCancel->hide();
      delete m_btnCancel;
   }

   void wndSettings::OnAlgorithmChanged(int index) {
      clustlib::algorithms nalg = utils::getAlgEnum(getAsCombo(ParameterAlgorithm)->currentIndex());
      collectTemp();
      auto& settings = m_TempSettings.m_Settings[utils::getAlgInt(nalg)];

      auto* spinEras = getAsSpin(ParameterErasCount);
      spinEras->setValue(settings.m_CommonSettings.eras);
      auto* spinThreshold = getAsDoubleSpin(ParameterThreshold);
      spinThreshold->setValue(settings.m_CommonSettings.threshold);
      auto* spinRate = getAsDoubleSpin(ParameterRate);
      spinRate->setValue(settings.m_CommonSettings.learning_rate);
      auto* spinDownturn = getAsDoubleSpin(ParameterDownturn);
      spinDownturn->setValue(settings.m_CommonSettings.rate_downturn);

      bool isCurDist = utils::isDistanceAlg(m_TempSettings.m_algCurrent);
      bool isNewDist = utils::isDistanceAlg(nalg);
      if (isNewDist == isCurDist) {
         // Theres only one non-dist algorithm, so they are probably both dist algorithms
         auto* spinCrit = getAsDoubleSpin(ParameterSpecial);
         spinCrit->setValue(settings.m_dCritDistance);
      }
      else {
         Parameter& parameter = m_arrParameters[ParameterSpecial];
         
         parameter.label->hide();
         parameter.widget->hide();
         delete parameter.label;
         delete parameter.widget;

         if (isNewDist) {
            parameter.ifc = IfcDoubleSpin;
            parameter.label = new QLabel(QString("Критическое расстояние:"), this);
            parameter.widget = new QDoubleSpinBox(this);

            auto* spinCrit = dynamic_cast <QDoubleSpinBox*> (parameter.widget);
            spinCrit->setMinimum(0.0);
            spinCrit->setSingleStep(0.01);
            spinCrit->setValue(settings.m_dCritDistance);
         }
         else {
            parameter.ifc = IfcSpin;
            parameter.label = new QLabel(QString("Количество кластеров:"), this);
            parameter.widget = new QSpinBox(this);
            
            auto* spinClusters = dynamic_cast <QSpinBox*> (parameter.widget);
            spinClusters->setMinimum(0);
            spinClusters->setSingleStep(1);
            spinClusters->setValue(settings.m_nClusters);
         }

         parameter.size = parameter.widget->size();
         parameter.lsize = parameter.label->size();

         const Parameter& prevParam = m_arrParameters[static_cast <int> (ParameterSpecial - 1)];
         parameter.label->move(prevParam.label->pos() + QPoint(0, parameter.label->height() - 3));

         parameter.widget->move(prevParam.widget->pos() + QPoint(0, parameter.widget->height() - 3));

         parameter.widget->show();
         parameter.label->show();
      }

      m_curSettings = &settings;
      m_TempSettings.m_algCurrent = nalg;
   }

   void wndSettings::OnApply() {
      collect();
      accept();
   }

   void wndSettings::OnCancel() {
      reject();
      reset();
   }

   void wndSettings::connect() {
      QObject::connect(m_btnApply, &QPushButton::clicked, this, &wndSettings::OnApply);
      QObject::connect(m_btnCancel, &QPushButton::clicked, this, &wndSettings::OnCancel);
      QObject::connect(dynamic_cast <QComboBox*> (m_arrParameters[ParameterAlgorithm].widget),
         QOverload<int>::of(&QComboBox::currentIndexChanged), this, &wndSettings::OnAlgorithmChanged);
   }

   void wndSettings::initParameters() {
      auto parameterPreInit = [](Parameter& current,
         ParameterInterface ifc, QLabel* label, QWidget* w) {
         current.ifc = ifc;
         current.label = label;
         current.widget = w;
      };

      auto parameterPostInit = [](Parameter& current) {
         current.label->show();
         current.widget->show();
         current.size = current.widget->size();
         current.lsize = current.label->size();
      };

      // Algorithm combo box
      auto& alg = m_arrParameters[ParameterAlgorithm];
      parameterPreInit(alg, IfcCombo, new QLabel(QString("Нейронная сеть:"), this), new QComboBox(this));
      {
         auto* comboAlg = dynamic_cast <QComboBox*> (alg.widget);
         comboAlg->addItem(QString("Самообучаемая сеть"),      QVariant(static_cast <int> (clustlib::algorithms::alg_kohonen)));
         comboAlg->addItem(QString("Самоорганизующаяся сеть"), QVariant(static_cast <int> (clustlib::algorithms::alg_kohonen_so)));
         comboAlg->addItem(QString("Иерархическая сеть"),      QVariant(static_cast <int> (clustlib::algorithms::alg_hierarchical_kohonen)));
         comboAlg->setCurrentIndex(utils::getAlgInt(m_Data.m_algCurrent));
      }
      parameterPostInit(alg);

      // Metric combo box
      auto& metric = m_arrParameters[ParameterMetric];
      parameterPreInit(metric, IfcCombo, new QLabel(QString("Метрика:"), this), new QComboBox(this));
      {
         using namespace utils;
         auto* comboMetric = dynamic_cast <QComboBox*> (metric.widget);
         comboMetric->addItem(QString("Евклидово расстояние"), QVariant(getMetricInt(euclidean)));
         comboMetric->addItem(QString("Квадрат Евклидового расстояния"), QVariant(getMetricInt(sqeuclidean)));
         comboMetric->addItem(QString("Расстояние городских кварталов"), QVariant(getMetricInt(manhattan)));
         comboMetric->addItem(QString("Расстояние Чебышева"), QVariant(getMetricInt(chebyshev)));
         comboMetric->setCurrentIndex(getMetricInt(getMetric(m_Data.m_funcMetric)));
      }
      parameterPostInit(metric);

      // Eras count spin box
      auto& eras = m_arrParameters[ParameterErasCount];
      parameterPreInit(eras, IfcSpin, new QLabel(QString("Количество эпох:"), this), new QSpinBox(this));
      {
         auto* spinEras = dynamic_cast <QSpinBox*> (eras.widget);
         spinEras->setMinimum(0);
         spinEras->setSingleStep(1);
         spinEras->setValue(m_curSettings->m_CommonSettings.eras);
      }
      parameterPostInit(eras);

      // Threshold spin box
      auto& threshold = m_arrParameters[ParameterThreshold];
      parameterPreInit(threshold, IfcDoubleSpin, new QLabel(QString("Порог обучения:"), this), new QDoubleSpinBox(this));
      {
         auto* spinThreshold = dynamic_cast <QDoubleSpinBox*> (threshold.widget);
         spinThreshold->setMinimum(0.0);
         spinThreshold->setSingleStep(0.01);
         spinThreshold->setValue(m_curSettings->m_CommonSettings.threshold);
      }
      parameterPostInit(threshold);
      
      // Learning rate spin box
      auto& rate = m_arrParameters[ParameterRate];
      parameterPreInit(rate, IfcDoubleSpin, new QLabel(QString("Норма обучения:"), this), new QDoubleSpinBox(this));
      {
         auto* spinRate = dynamic_cast <QDoubleSpinBox*> (rate.widget);
         spinRate->setMinimum(0.0);
         spinRate->setSingleStep(0.01);
         spinRate->setMaximum(1.0);
         spinRate->setValue(m_curSettings->m_CommonSettings.learning_rate);
      }
      parameterPostInit(rate);

      // Learning downturn spin box
      auto& downturn = m_arrParameters[ParameterDownturn];
      parameterPreInit(downturn, IfcDoubleSpin, new QLabel(QString("Спуск нормы обучения:"), this), new QDoubleSpinBox(this));
      {
         auto* spinDownturn = dynamic_cast <QDoubleSpinBox*> (downturn.widget);
         spinDownturn->setMinimum(0.0);
         spinDownturn->setSingleStep(0.01);
         spinDownturn->setValue(m_curSettings->m_CommonSettings.rate_downturn);
      }
      parameterPostInit(downturn);

      // Algorithm special parameter
      auto& special = m_arrParameters[ParameterSpecial];
      if (!utils::isDistanceAlg(m_Data.m_algCurrent)) {
         parameterPreInit(special, IfcSpin, new QLabel(QString("Количество кластеров:"), this), new QSpinBox(this));
         {
            QSpinBox* spinClusters = dynamic_cast <QSpinBox*> (special.widget);
            spinClusters->setMinimum(0);
            spinClusters->setSingleStep(1);
            spinClusters->setValue(m_curSettings->m_nClusters);
         }
      }
      else {
         parameterPreInit(special, IfcDoubleSpin, new QLabel(QString("Критическое расстояние:"), this), new QDoubleSpinBox(this));
         {
            QDoubleSpinBox* spinCrit = dynamic_cast <QDoubleSpinBox*> (special.widget);
            spinCrit->setMinimum(0.0);
            spinCrit->setSingleStep(0.01);
            spinCrit->setValue(m_curSettings->m_dCritDistance);
         }
      }
      parameterPostInit(special);
   }

   void wndSettings::resizeEvent(QResizeEvent* event) {
      const QSize& size = event->size();

      constexpr auto margin = 7;
      constexpr int heightBtn = 25;
      constexpr int widthBtn = 80;

      auto y = margin;
      for (auto i = 0; i < ParametersCount; ++i) {
         Parameter& cur = m_arrParameters[i];
         cur.label->move(margin, y);
         cur.widget->move(size.width() - (cur.size.width() + margin), y - (margin / 2));
         
         y += std::max(cur.size.height(), cur.lsize.height());
         y += margin;
      }

      m_btnCancel->resize(widthBtn, heightBtn);
      m_btnCancel->move(size.width() - (margin + widthBtn), size.height() - (margin + heightBtn));
      m_btnApply->resize(widthBtn, heightBtn);
      m_btnApply->move(size.width() - 2 * (margin + widthBtn), size.height() - (margin + heightBtn));
   }

   void wndSettings::collectCommon() {
      auto* spinEras = getAsSpin(ParameterErasCount);
      auto* spinThreshold = getAsDoubleSpin(ParameterThreshold);
      auto* spinRate = getAsDoubleSpin(ParameterRate);
      auto* spinDownturn = getAsDoubleSpin(ParameterDownturn);

      m_curSettings->m_CommonSettings.eras = spinEras->value();
      m_curSettings->m_CommonSettings.threshold = spinThreshold->value();
      m_curSettings->m_CommonSettings.learning_rate = spinRate->value();
      m_curSettings->m_CommonSettings.rate_downturn = spinDownturn->value();
   }

   void wndSettings::collectSpecial() {
      if (!utils::isDistanceAlg(m_TempSettings.m_algCurrent)) {
         auto* spinClusters = getAsSpin(ParameterSpecial);
         m_curSettings->m_nClusters = spinClusters->value();
      }
      else {
         auto* spinCrit = getAsDoubleSpin(ParameterSpecial);
         m_curSettings->m_dCritDistance = spinCrit->value();
      }
   }

   void wndSettings::collectTemp() {
      collectCommon();
      collectSpecial();
   }

   void wndSettings::collect() {
      collectTemp();
      m_TempSettings.m_funcMetric = utils::getMetric(utils::getMetricEnum(getAsCombo(ParameterMetric)->currentIndex()));
      m_Data = m_TempSettings;
   }

   void wndSettings::resetCommon() {
      auto* comboAlg = getAsCombo(ParameterAlgorithm);
      comboAlg->setCurrentIndex(utils::getAlgInt(m_TempSettings.m_algCurrent));
      auto* comboMetric = getAsCombo(ParameterMetric);
      comboMetric->setCurrentIndex(utils::getMetricInt(utils::getMetric(m_TempSettings.m_funcMetric)));

      auto* spinEras = getAsSpin(ParameterErasCount);
      spinEras->setValue(m_curSettings->m_CommonSettings.eras);
      auto* spinThreshold = getAsDoubleSpin(ParameterThreshold);
      spinThreshold->setValue(m_curSettings->m_CommonSettings.threshold);
      auto* spinRate = getAsDoubleSpin(ParameterRate);
      spinRate->setValue(m_curSettings->m_CommonSettings.learning_rate);
      auto* spinDownturn = getAsDoubleSpin(ParameterDownturn);
      spinDownturn->setValue(m_curSettings->m_CommonSettings.rate_downturn);
   }

   void wndSettings::resetSpecial() {
      Parameter& parameter = m_arrParameters[ParameterSpecial];

      parameter.label->hide();
      parameter.widget->hide();
      delete parameter.label;
      delete parameter.widget;

      if (utils::isDistanceAlg(m_TempSettings.m_algCurrent)) {
         parameter.ifc = IfcDoubleSpin;
         parameter.label = new QLabel(QString("Критическое расстояние:"), this);
         parameter.widget = new QDoubleSpinBox(this);

         auto* spinCrit = dynamic_cast <QDoubleSpinBox*> (parameter.widget);
         spinCrit->setMinimum(0.0);
         spinCrit->setSingleStep(0.01);
         spinCrit->setValue(m_curSettings->m_dCritDistance);
      }
      else {
         parameter.ifc = IfcSpin;
         parameter.label = new QLabel(QString("Количество кластеров:"), this);
         parameter.widget = new QSpinBox(this);

         auto* spinClusters = dynamic_cast <QSpinBox*> (parameter.widget);
         spinClusters->setMinimum(0);
         spinClusters->setSingleStep(1);
         spinClusters->setValue(m_curSettings->m_nClusters);
      }

      parameter.size = parameter.widget->size();
      parameter.lsize = parameter.label->size();

      const Parameter& prevParam = m_arrParameters[static_cast <int> (ParameterSpecial - 1)];
      parameter.label->move(prevParam.label->pos() + QPoint(0, parameter.label->height() - 3));

      parameter.widget->move(prevParam.widget->pos() + QPoint(0, parameter.widget->height() - 3));

      parameter.widget->show();
      parameter.label->show();
   }

   void wndSettings::reset() {
      m_TempSettings = m_Data;
      m_curSettings = &m_TempSettings.m_Settings[utils::getAlgInt(m_TempSettings.m_algCurrent)];
      resetCommon();
      resetSpecial();
   }

   void wndSettings::disconnect() {
      QObject::disconnect(m_btnApply, &QPushButton::clicked, this, &wndSettings::OnApply);
      QObject::disconnect(m_btnCancel, &QPushButton::clicked, this, &wndSettings::OnCancel);
      QObject::disconnect(dynamic_cast <QComboBox*> (m_arrParameters[ParameterAlgorithm].widget),
         QOverload<int>::of(&QComboBox::currentIndexChanged), this, &wndSettings::OnAlgorithmChanged);
   }

   QSpinBox* wndSettings::getAsSpin(Parameters p) {
      return dynamic_cast <QSpinBox*> (m_arrParameters[p].widget);
   }

   QDoubleSpinBox* wndSettings::getAsDoubleSpin(Parameters p) {
      return dynamic_cast <QDoubleSpinBox*> (m_arrParameters[p].widget);
   }

   QComboBox* wndSettings::getAsCombo(Parameters p) {
      return dynamic_cast <QComboBox*> (m_arrParameters[p].widget);
   }
}
