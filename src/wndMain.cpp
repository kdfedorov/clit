#include "include/wndMain.h"
#include "include/wndSettings.h"
#include "include/wndReport.h"

#include <QResizeEvent>
#include <QMenuBar>
#include <QFileDialog>
#include <QMessageBox>

namespace clit {
   wndMain::wndMain(int width/*= 640*/, int height/*= 480*/) 
      : m_csvManager(""), m_barMenu(nullptr) {
      this->setWindowTitle("clit");
      this->setMinimumSize(190, 50);

      m_btnLaunch = new QPushButton(QString("Запустить"), this);
      m_btnSettings = new QPushButton(QString("Настройка"), this);
      m_tableDataset = new QTableWidget(this);

      initMenubar();
      this->resize(width, height);
      OnNewFile();

      connect();
   }

   wndMain::~wndMain() {
      disconnect();

      delete m_tableDataset;
      delete m_btnLaunch;
      delete m_btnSettings;

      m_barMenu = nullptr;
      m_csvManager.close();
   }

   void wndMain::OnButtonLaunch() {
      wndReport* windowReport = nullptr;
      setCursor(Qt::WaitCursor);
      
      clustlib::dataset ds;
      m_csvManager.read(ds);
      if (ds.empty()) {
         QMessageBox::information(this, QString("Пустая выборка"),
            QString("Входное множество является пустым и/или не состоит из состоятельных данных!"));
         return;
      }

      auto typeAlg = m_Data.m_algCurrent;
      auto Settings = m_Data.m_Settings[static_cast <int>(typeAlg)];

      if (Settings.m_CommonSettings.eras < 1) {
         QMessageBox::information(this, QString("Некорректные параметры сети"), QString("Необходимо задать количество эпох!"));
         return;
      }

      if (Settings.m_CommonSettings.learning_rate <= 0.0) {
         QMessageBox::information(this, QString("Некорректные параметры сети"), QString("Необходимо задать норму обучения!"));
         return;
      }

      clustlib::cluster_vec   cv;
      clustlib::cluster_tree  ct;
      switch (typeAlg) {
         case clustlib::algorithms::alg_kohonen: {
            if (Settings.m_nClusters < 2) {
               QMessageBox::information(this, QString("Некорректные параметры сети"), QString("Задано некорректное количество кластеров!"));
               return;
            }

            clustlib::kohonen network(&ds, true, Settings.m_CommonSettings, Settings.m_nClusters, m_Data.m_funcMetric);
            if (!network.clusterize()) {
               QMessageBox::information(this, QString("Информация"), QString("Кластеризация не удалась."));
               return;
            }

            network.result(cv);
            windowReport = new wndReport(cv, this);

            break;
         }
         case clustlib::algorithms::alg_kohonen_so: {
            if (Settings.m_dCritDistance <= 0.0) {
               QMessageBox::information(this, QString("Некорректные параметры сети"), QString("Необходимо задать критическое расстояние!"));
               return;
            }

            clustlib::kohonen_so network(&ds, true, Settings.m_CommonSettings, Settings.m_dCritDistance, m_Data.m_funcMetric);
            if (!network.clusterize()) {
               QMessageBox::information(this, QString("Информация"), QString("Кластеризация не удалась."));
               return;
            }

            network.result(cv);
            windowReport = new wndReport(cv, this);

            break;
         }
         case clustlib::algorithms::alg_hierarchical_kohonen: {
            if (Settings.m_dCritDistance <= 0.0) {
               QMessageBox::information(this, QString("Некорректные параметры сети"), QString("Необходимо задать критическое расстояние!"));
               return;
            }

            clustlib::hierarchical_kohonen network(&ds, Settings.m_CommonSettings, Settings.m_dCritDistance, m_Data.m_funcMetric);
            if (!network.clusterize()) {
               QMessageBox::information(this, QString("Информация"), QString("Кластеризация не удалась."));
               return;
            }

            network.result(ct);
            windowReport = new wndReport(ct, this);

            break;
         }
         default: {
            QMessageBox::warning(this, QString("Ошибка"), QString("Неизвестный алгоритм кластеризации!"));
            setCursor(Qt::ArrowCursor);
            return;
         }
      }

      setCursor(Qt::ArrowCursor);
      windowReport->exec();
      delete windowReport;
   }

   void wndMain::OnButtonSettings() {
      auto* windowSettings = new wndSettings(m_Data, this);
      windowSettings->exec();
      delete windowSettings;
   }

   void wndMain::OnNewFile() {
      if (!m_bSaved)
         if (!handleUnsaved())
            return;

      m_csvManager.close();
      m_strFile = "";
      updateTable();
      this->setWindowTitle(QString("clit - new.csv"));
      m_bSaved = true;
   }

   void wndMain::OnOpenFile() {
      if (!m_bSaved)
         if (!handleUnsaved())
            return;

      QString filename = QFileDialog::getOpenFileName(this, QString("Открыть файл"), QString(), "*.csv");
      if (filename.isEmpty()) {
         QMessageBox::information(this, QString("Информация"), QString("Не удалось открыть файл"));
         OnNewFile();
      }
      else {
         m_strFile = filename;
         markAsSaved();
         updateTable();
      }
   }

   void wndMain::OnSaveFile() {
      if (m_strFile.isEmpty()) {
         OnSaveAs();
         return;
      }

      if (m_csvManager.write(m_strFile.toStdString().c_str())) 
         markAsSaved();
      else
         QMessageBox::information(this, QString("Информация"), QString("Не удалось сохранить файл"));
   }

   void wndMain::OnSaveAs() {
      QString filename = QFileDialog::getSaveFileName(this, QString("Сохранить как..."), QString(), "*.csv");
      bool success = filename.isEmpty();
      if (success) {
         m_strFile = filename;
         success = m_csvManager.write(m_strFile.toStdString().c_str());
         if (success)
            markAsSaved();
      }

      if (!success)
         QMessageBox::information(this, QString("Информация"), QString("Не удалось сохранить файл"));
   }

   void wndMain::OnAddRow() {
      m_csvManager.insert_row();
      m_tableDataset->insertRow(m_tableDataset->rowCount());
      markAsUnsaved();
   }

   void wndMain::OnAddColumn() {
      m_csvManager.insert_column();
      m_tableDataset->insertColumn(m_tableDataset->columnCount());
      markAsUnsaved();
   }

   void wndMain::OnRemoveRow() {
      m_csvManager.cutline(m_csvManager.linecount() - 1);
      m_tableDataset->removeRow(m_tableDataset->rowCount() - 1);
      markAsUnsaved();
   }

   void wndMain::OnRemoveColumn() {
      m_csvManager.cutcolumn(m_csvManager.attrcount() - 1);
      m_tableDataset->removeColumn(m_tableDataset->columnCount() - 1);
      markAsUnsaved();
   }

   void wndMain::connect() {
      // Push button connect
      QObject::connect(m_btnLaunch, &QPushButton::clicked, this, &wndMain::OnButtonLaunch);
      QObject::connect(m_btnSettings, &QPushButton::clicked, this, &wndMain::OnButtonSettings);

      // File actions group connect
      QObject::connect(m_arrActions[FileGroup][NewFileAction], &QAction::triggered, this, &wndMain::OnNewFile);
      QObject::connect(m_arrActions[FileGroup][OpenFileAction], &QAction::triggered, this, &wndMain::OnOpenFile);
      QObject::connect(m_arrActions[FileGroup][SaveFileAction], &QAction::triggered, this, &wndMain::OnSaveFile);
      QObject::connect(m_arrActions[FileGroup][SaveAsAction], &QAction::triggered, this, &wndMain::OnSaveAs);

      // Table actions group connect
      QObject::connect(m_arrActions[TableGroup][AddRowAction], &QAction::triggered, this, &wndMain::OnAddRow);
      QObject::connect(m_arrActions[TableGroup][AddColumnAction], &QAction::triggered, this, &wndMain::OnAddColumn);
      QObject::connect(m_arrActions[TableGroup][RemoveRowAction], &QAction::triggered, this, &wndMain::OnRemoveRow);
      QObject::connect(m_arrActions[TableGroup][RemoveColumnAction], &QAction::triggered, this, &wndMain::OnRemoveColumn);

      // Other
      QObject::connect(m_tableDataset, &QTableWidget::cellChanged, this, &wndMain::onCellChanged);
   }

   void wndMain::initMenubar() {
      if (m_barMenu)
         delete m_barMenu;

      initActions();
      m_barMenu = new QMenuBar(this);

      auto* menuFile = new QMenu(QString("Файл"), this);
      menuFile->addAction(m_arrActions[FileGroup][NewFileAction]);
      menuFile->addAction(m_arrActions[FileGroup][OpenFileAction]);
      menuFile->addSeparator();
      menuFile->addAction(m_arrActions[FileGroup][SaveFileAction]);
      menuFile->addAction(m_arrActions[FileGroup][SaveAsAction]);

      auto* menuTable = new QMenu(QString("Таблица"), this);
      menuTable->addAction(m_arrActions[TableGroup][AddRowAction]);
      menuTable->addAction(m_arrActions[TableGroup][AddColumnAction]);
      menuTable->addSeparator();
      menuTable->addAction(m_arrActions[TableGroup][RemoveRowAction]);
      menuTable->addAction(m_arrActions[TableGroup][RemoveColumnAction]);

      m_barMenu->addMenu(menuFile);
      m_barMenu->addMenu(menuTable);
      m_barMenu->show();
   }

   void wndMain::initActions() {
      for (auto& group : m_arrActions)
         group.clear();

      initFileGroup();
      initTableGroup();
   }

   void wndMain::initFileGroup() {
      ActionsVector vecActions;
      for (auto i = 0; i < FileActionsCount; ++i)
         vecActions.push_back(nullptr);

      vecActions[NewFileAction] = new QAction(QString("Новый"), this);
      vecActions[OpenFileAction] = new QAction(QString("Открыть"), this);
      vecActions[SaveFileAction] = new QAction(QString("Сохранить"), this);
      vecActions[SaveAsAction] = new QAction(QString("Сохранить как"), this);

      m_arrActions[FileGroup] = vecActions;
   }

   void wndMain::initTableGroup() {
      ActionsVector vecActions;
      for (auto i = 0; i < TableActionsCount; ++i)
         vecActions.push_back(nullptr);

      vecActions[AddRowAction] = new QAction(QString("Добавить строку"), this);
      vecActions[AddColumnAction] = new QAction(QString("Добавить столбец"), this);
      vecActions[RemoveRowAction] = new QAction(QString("Убрать строку"), this);
      vecActions[RemoveColumnAction] = new QAction(QString("Убрать столбец"), this);

      m_arrActions[TableGroup] = vecActions;
   }

   void wndMain::updateTable() {
      if (m_strFile.isEmpty())
         return;

      m_bUpdate = true;
      m_csvManager.open(m_strFile.toStdString().c_str());

      m_tableDataset->clear();
      int rows = m_csvManager.linecount();
      int cols = m_csvManager.attrcount();
      m_tableDataset->setRowCount(rows);
      m_tableDataset->setColumnCount(cols);

      clustlib::csv::table_content tc;
      m_csvManager.content(tc);

      for (auto row = 0; row < rows; ++row) {
         for (auto col = 0; col < cols; ++col) {
            QString val = QString(tc[row][col].c_str());
            m_tableDataset->setItem(row, col, new QTableWidgetItem(val));
         }
      }

      m_bUpdate = false;
   }

   void wndMain::markAsSaved() {
      if (m_bSaved)
         return;
      m_bSaved = true;
      this->setWindowTitle(QString("clit - ") + m_strFile);
   }

   void wndMain::markAsUnsaved() {
      if (!m_bSaved)
         return;
      m_bSaved = false;
      this->setWindowTitle(QString("clit - ") + m_strFile + '*');
   }

   bool wndMain::handleUnsaved() {
      auto reply = QMessageBox::question(this, QString("Несохраненные изменения"),
         QString("Изменения, которые Вы внесли несохранены. Сохранить?"),
         QMessageBox::StandardButton::Yes | QMessageBox::StandardButton::No | QMessageBox::StandardButton::Cancel);

      if (reply == QMessageBox::StandardButton::Cancel)
         return false;
      else if (reply == QMessageBox::StandardButton::Yes)
         OnSaveFile();

      markAsSaved();
      return m_bSaved;
   }

   void wndMain::onCellChanged(int row, int column) {
      if (m_bUpdate)
         return;

      auto* val = m_tableDataset->item(row, column);
      std::string str = val->text().toStdString();
      m_csvManager.set(row, column, str);
      markAsUnsaved();
   }

   void wndMain::resizeEvent(QResizeEvent* event) {
      const QSize& size = event->size();
      constexpr int margin = 7;
      constexpr int heightBtn = 25;
      constexpr int widthBtn = 80;
      constexpr int heightMenu = 20;

      this->resize(size);

      m_barMenu->move(0, 0);
      m_barMenu->resize(size.width(), heightMenu);

      m_tableDataset->move(margin, margin + heightMenu);
      m_tableDataset->resize(size.width() - (margin * 2), size.height() - (margin * 3 + heightBtn + heightMenu));

      m_btnSettings->move(margin, size.height() - (margin + heightBtn));
      m_btnSettings->resize(widthBtn, heightBtn);

      m_btnLaunch->move(size.width() - (margin + widthBtn), size.height() - (margin + heightBtn));
      m_btnLaunch->resize(widthBtn, heightBtn);
   }

   void wndMain::disconnect() {
      // Push button connect
      QObject::disconnect(m_btnLaunch, &QPushButton::clicked, this, &wndMain::OnButtonLaunch);
      QObject::disconnect(m_btnSettings, &QPushButton::clicked, this, &wndMain::OnButtonSettings);

      // File actions group connect
      QObject::disconnect(m_arrActions[FileGroup][NewFileAction], &QAction::triggered, this, &wndMain::OnNewFile);
      QObject::disconnect(m_arrActions[FileGroup][OpenFileAction], &QAction::triggered, this, &wndMain::OnOpenFile);
      QObject::disconnect(m_arrActions[FileGroup][SaveFileAction], &QAction::triggered, this, &wndMain::OnSaveFile);
      QObject::disconnect(m_arrActions[FileGroup][SaveAsAction], &QAction::triggered, this, &wndMain::OnSaveAs);

      // Table actions group connect
      QObject::disconnect(m_arrActions[TableGroup][AddRowAction], &QAction::triggered, this, &wndMain::OnAddRow);
      QObject::disconnect(m_arrActions[TableGroup][AddColumnAction], &QAction::triggered, this, &wndMain::OnAddColumn);
      QObject::disconnect(m_arrActions[TableGroup][RemoveRowAction], &QAction::triggered, this, &wndMain::OnRemoveRow);
      QObject::disconnect(m_arrActions[TableGroup][RemoveColumnAction], &QAction::triggered, this, &wndMain::OnRemoveColumn);

      // Other
      QObject::disconnect(m_tableDataset, &QTableWidget::cellChanged, this, &wndMain::onCellChanged);
   }
}
